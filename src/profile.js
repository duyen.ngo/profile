import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableHighlight, Alert, TextInput} from 'react-native';
import { Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign'

export default class profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View>
            <View style={styles.profileWrap}>
                <Image style={{width: '100%', height: 270, opacity: 0.5,}} source={{uri: 'https://hdqwalls.com/download/minimalism-birds-mountains-trees-forest-9k-1920x1080.jpg'}}/>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
                    <Avatar
                        size= 'xlarge'
                        rounded 
                        source={{
                        uri:
                            'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                        }}
                    />
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start'}}>
                <View style={styles.phone}>
                    <Icon style={{marginTop: 100, marginLeft: 20,}} name="user" size={25} color={'#3ee2bf'}/>
                </View>
                <View style={styles.phoneNumber}>
                    <Text style={{marginTop: 105, marginLeft: 15}}>Duyen Ngo</Text>
                </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start'}}>
                <View style={styles.phone}>
                    <Icon style={{marginTop: 140, marginLeft: 20,}} name="phone" size={25} color={'#3ee2bf'}/>
                </View>
                <View style={styles.phoneNumber}>
                    <Text style={{marginTop: 145, marginLeft: 15}}>0903524510</Text>
                </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start'}}>
                <View style={styles.email}>
                    <Icon style={{marginTop: 180, marginLeft: 20,}} name="mail" size={25} color={'#3ee2bf'}/>
                </View>
                <View style={styles.emailAddress}>
                    <Text style={{marginTop: 185, marginLeft: 15}}>nthanhduyen597@gmail.com</Text>
                </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start'}}>
                <View style={styles.place}>
                    <Icon style={{marginTop: 220, marginLeft: 20,}} name="home" size={25} color={'#3ee2bf'}/>
                </View>
                <View style={styles.placeAddress}>
                    <Text style={{marginTop: 225, marginLeft: 15}}>381 Le Duan, Thanh Khe, Da Nang</Text>
                </View>
            </View>
        </View>
    );
  }
}


const styles = StyleSheet.create({

})




