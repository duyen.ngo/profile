import React, { Component } from 'react';
import { View, Text, StyleSheet, Switch, TouchableHighlight,Alert } from 'react-native';
import { Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign'



export default class settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  state = {
    pushNotifications: true,
  }

  onChangePushNotifications = () => {
    this.setState(state => ({
      pushNotifications: !state.pushNotifications,
    }))
  }

  render() {
    return (
      <View>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10,}}>
                <View style={styles.userAccount}>
                    <Avatar
                        size= 'large'
                        rounded 
                        source={{
                            uri:
                            'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                        }}
                    />
                </View>
                <View style={styles.userInfo}>
                    <Text style={{fontWeight: 'bold', fontSize: 18}}>Duyen Ngo</Text>
                    <Text style={{fontSize: 18}}>nthanhduyen597@gmail.com</Text>
                </View>
          </View>
          <View style={{height: 50, width: '100%', backgroundColor: '#C0C0C0', opacity: 0.4 ,marginTop: 90}}>
                <Text style={{fontSize: 18, marginTop: 10, fontWeight: 'bold', marginLeft: 10}}>Settings</Text>
          </View>
          <View style={styles.ListItems}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={styles.notice}>
                        <Icon style={{marginTop: 15, marginLeft: 15, }} name="notification" size={30} />
                    </View>
                    <View style={styles.NoticeTxt}>
                        <Text style={{marginTop: 18, fontSize: 18, marginRight: 73}}>Notifications</Text>
                    </View>
                    <View style={styles.switchBtn}>
                        <Switch
                            onValueChange={this.onChangePushNotifications}
                            value={this.state.pushNotifications}
                        />
                    </View>
                </View>

                <View style={{borderBottomWidth: 1, borderBottomColor: '#A7B0B1', marginTop: 65}}></View>

                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={styles.notice}>
                            <Icon style={{marginTop: 20, marginLeft: 15 }} name="setting" size={30} />
                        </View>
                        <View style={styles.NoticeTxt}>
                            <Text style={{marginTop: 20, fontSize: 18, marginRight: 105}}>Language</Text>
                        </View>
                        <View style={styles.LangSetting}>
                            <TouchableHighlight underlayColor = {'red'} onPress = {() => Alert.alert('You tapped on the button')}>
                                <Icon style={{marginTop: 20, marginRight: 10}} name="right" size={25}/> 
                            </TouchableHighlight>
                        </View>
                </View>

                <View style={{borderBottomWidth: 1, borderBottomColor: '#A7B0B1', marginTop: 70}}></View>

                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={styles.notice}>
                            <Icon style={{marginTop: 25, marginLeft: 15 }} name="user" size={30} />
                        </View>
                        <View style={styles.NoticeTxt}>
                            <Text style={{marginTop: 25, fontSize: 18, marginRight: 97}}>Edit Profile</Text>
                        </View>
                        <View style={styles.LangSetting}>
                            <TouchableHighlight underlayColor = {'red'} onPress = {() => Alert.alert('You tapped on the button')}>
                                <Icon style={{marginTop: 25, marginRight: 10}} name="right" size={25}/> 
                            </TouchableHighlight>
                        </View>
                </View>

                <View style={{borderBottomWidth: 1, borderBottomColor: '#A7B0B1', marginTop: 75}}></View>

                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={styles.notice}>
                            <Icon style={{marginTop: 190, marginLeft: 15 }} name="logout" size={30}  />
                        </View>
                        <View style = {styles.container}>
                            <TouchableHighlight underlayColor = {'red'} onPress = {() => Alert.alert('You tapped on the button')}>
                                <Text style = {{marginTop: 190, fontSize: 18, marginRight: 235}}>
                                Logout
                                </Text>
                            </TouchableHighlight>
                        </View>
                </View>
          </View>
          {/* <View style = {styles.container}>
            <TouchableHighlight underlayColor = {'red'} onPress = {() => Alert.alert('You tapped on the button')}>
                <Text style = {styles.button}>
                Button
                </Text>
            </TouchableHighlight>
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
    userInfo: {
        marginLeft: 15,
        marginTop: 10
    },

    userAccount: {
        marginLeft: 15
    },

    switchBtn: {
        marginTop: 15,
    },
})